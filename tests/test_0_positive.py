import pytest
from fabrique_nodes_core.validators import validate_node
from fabrique_nodes_core.convertors import convert_node_data_to_config
from fabrique_nodes_core import NodeData, root_port, is_valid_port
from fabrique_nodes_core.db.fake_db import FakeDB
from unittest.mock import patch

import os
import sys

from dotenv import load_dotenv
load_dotenv()

cur_file_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f"{cur_file_dir}/..")


from src.node import Node, KafkaRead, KafkaWrite
from src.node import NodesGroup


assert issubclass(Node, NodesGroup)

node_classes = {n.type_: n for n in Node.nodes_array}
assert KafkaRead == node_classes["KafkaRead"]
assert KafkaWrite == node_classes["KafkaWrite"]


@pytest.mark.parametrize("node", (KafkaRead, KafkaWrite))
def test_positive_validations(node):
    validate_node(node)


@patch('fabrique_nodes_core.crypto.DB', new=FakeDB)
def test_general():
    API_KEY = os.environ['API_KEY']
    API_SECRET = os.environ['API_SECRET']
    SERVER_SOCKETS = os.environ['SERVER_SOCKETS']
    GROUP_ID = 'pytest'
    TOPIC_NAME = 'test_topic'
    AUTO_OFFSET_RESET = 'earliest'
    SESSION_TIMEOUT_MS = '45000'

    user_id = '_user_id_'

    ui_config = {
        "topic_name": TOPIC_NAME,
        "api_key": API_KEY,
        "api_secret_key": API_SECRET,
        "server_sockets": SERVER_SOCKETS,
        "security_protocol": "SASL_SSL",
        "sasl_mechanisms": "PLAIN"
    }

    ui_consumer_config = {**ui_config,
                          "group_id": GROUP_ID,
                          'auto_offset_reset': AUTO_OFFSET_RESET,
                          'session_timeout_ms': str(SESSION_TIMEOUT_MS)}

    node_data_read = NodeData(
        name="",
        g_ports_out=[[root_port], [is_valid_port]],
        ui_config=ui_consumer_config.copy()
    )

    node_data_write = NodeData(
        name="",
        g_ports_in=[[root_port], [is_valid_port]],
        ui_config=ui_config.copy()
    )

    # cfg_read = convert_node_data_to_config(node_data_read)
    # cfg_write = convert_node_data_to_config(node_data_write)

    upd_node_data_read = KafkaRead.make_hashed_outputs_cb(node_data_read, user_id)
    upd_node_data_write = KafkaRead.make_hashed_outputs_cb(node_data_write, user_id)

    kafka_read = KafkaRead(convert_node_data_to_config(upd_node_data_read), user_id)
    kafka_write = KafkaWrite(convert_node_data_to_config(upd_node_data_write), user_id)
    # inputs = node.process(*input_val)
    test_values = [[{'key': 'value0'}, {'key': 'value1'}, {'key': 'value2'}]]
    kafka_write.process_many(test_values)
    result = kafka_read.process_many([[]])
    assert len(result) == len(test_values[0])
    assert all([r[0] in test_values[0] for r in result])
    print(result)

    # assert json.dumps(inputs) == json.dumps(expected_inputs)
