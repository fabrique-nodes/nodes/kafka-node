import os
import sys
from typing import Any, Literal
import json

from confluent_kafka import Producer, Consumer

from fabrique_nodes_core import (
    BaseNode,
    NodesGroup,
    NodeData,
    UIParams,
    Array,
    NodeConfig,
    root_port,
    is_valid_port,
    DestructurerUIParams,
    StructurerUIParams,
    Crypto,
    EmptyValue
)
from fabrique_nodes_core.convertors import upd_jspath_output

from pydantic import BaseModel, Field
import jmespath

cur_file_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f"{cur_file_dir}/..")


class UIConfig(BaseModel):
    topic_name: str = Field("topic name", title="Topic name", description="Kafka topic name")
    api_key: str = Field("", title="API key", description="API key (sasl.username)")
    api_secret_key: str = Field("", title="API secret key", description="API secret key (sasl.password)",
                                node_updater_callback_name="make_hashed_outputs_cb")
    access_key_hash: str = Field("",
                                 title="API secret key hash",
                                 description="API secret key hash (sasl.password hash)",
                                 visible=False
                                 )
    server_sockets: str = Field("", title="Server sockets", description="Server sockets (bootstrap.servers)")
    security_protocol: Literal["SASL_SSL", "SASL_PLAINTEXT"] = Field("SASL_SSL",
                                                                     title="Security protocol",
                                                                     description="Security protocol (security.protocol)"
                                                                     )
    sasl_mechanisms: Literal["PLAIN", "SCRAM-SHA-256", "SCRAM-SHA-512"] = Field(
        "PLAIN",
        title="SASL mechanisms",
        description="SASL mechanisms (sasl.mechanisms)"
    )


class UIConfigConsumer(UIConfig):
    """
        SESSION_TIMEOUT_MS = 45000
        AUTO_OFFSET_RESET = 'earliest'
        GROUP_ID = 'fabrique'
    """
    session_timeout_ms: int = Field(45000, title="Session timeout ms",
                                    description="Session timeout ms (session.timeout.ms)")

    auto_offset_reset: Literal["earliest", "latest"] = Field("earliest",
                                                             title="Auto offset reset",
                                                             description="Auto offset reset (auto.offset.reset)"
                                                             )
    group_id: str = Field(
        "",
        title="Group ID",
        description="Group ID (sasl.mechanisms)"
    )


class KafkaBase(BaseNode):
    def __init__(self, cfg: NodeConfig, user_id: str):
        super().__init__(cfg, user_id)
        self.ui_config = UIConfig(**cfg.ui_config)
        crypto = Crypto(user_id)
        access_key = crypto.decrypt(self.ui_config.access_key_hash)
        self.kafka_cfg = {
            'bootstrap.servers': self.ui_config.server_sockets,
            'security.protocol': self.ui_config.security_protocol,
            'sasl.mechanisms': self.ui_config.sasl_mechanisms,
            'sasl.username': self.ui_config.api_key,
            'sasl.password': access_key,
        }
        self.topic = self.ui_config.topic_name

    @classmethod
    def make_hashed_outputs_cb(cls, data: NodeData, user_id: str) -> NodeData:
        if data.ui_config['api_secret_key'] == '':
            return data  # do nothing
        crypto = Crypto(user_id)
        ui_config = UIConfig(**data.ui_config)
        data.ui_config['access_key_hash'] = crypto.encrypt(ui_config.api_secret_key)
        data.ui_config['api_secret_key'] = ''
        return data


class KafkaWrite(KafkaBase):
    """"""
    type_ = "KafkaWrite"
    category = "IO"

    initial_config = NodeData(
        name="KafkaWrite",
        g_ports_in=[
            [root_port],
        ],
        description="",
        ui_config=UIConfig(),
    )
    ui_params = UIParams(
        input_groups=[
            StructurerUIParams().input_groups[0],
        ],
        ui_config_schema=UIConfig.schema_json(),
    )

    def __init__(self, cfg: NodeConfig, user_id: str):
        super().__init__(cfg, user_id)
        self.producer = Producer(self.kafka_cfg)

    def process(self, *inputs) -> str:
        """Create json string

        :return: json string
        :rtype: str
        """
        output = {}
        for i, p in enumerate([p for p in self.cfg.ports_in if p.visible]):
            if not p.code:
                output = {**output, **inputs[i]}
                continue
            if p.special:  # jspath
                upd_jspath_output(output, p.code, inputs[i])
            else:  # model port case
                output[p.code] = inputs[i]
        return [output]

    def process_universal(self, *array_args) -> list:
        nonnull_args = [a for a in array_args if a]
        if nonnull_args and isinstance(nonnull_args[0], Array):
            results_list = [self.process_universal(*args) for args in zip(*array_args) if self.is_valid_args(args)]
            return [value for values_lst in zip(*results_list) for value in values_lst]
        else:
            return self.process(*array_args)

    def process_many(self, microbatch) -> list:
        print('process_many KafkaWrite microbatch', microbatch)
        values_lst = [self.process_universal(*args) for args in zip(*microbatch) if self.is_valid_args(args)]

        messages = [val for val_array in values_lst if val_array for val in val_array]

        key = None
        for mes in messages:
            self.producer.produce(self.topic, key=key, value=json.dumps(mes).encode())
            print(f'> Kafka {mes}')

    def __del__(self):
        self.producer.flush()


class KafkaRead(KafkaBase):
    """"""

    type_ = "KafkaRead"
    category = "IO"
    initial_config = NodeData(
        name="KafkaRead",
        g_ports_out=[
            [root_port],
            [is_valid_port],
        ],
        description="",
        ui_config=UIConfigConsumer(),
    )
    ui_params = UIParams(
        output_groups=DestructurerUIParams().output_groups,
        ui_config_schema=UIConfigConsumer.schema_json(),
    )

    def __init__(self, cfg: NodeConfig, user_id: str):
        super().__init__(cfg, user_id)
        self.ui_config = UIConfigConsumer(**cfg.ui_config)

        kafka_cfg = {**self.kafka_cfg,
                     "group.id": self.ui_config.group_id,
                     'auto.offset.reset': self.ui_config.auto_offset_reset,
                     'session.timeout.ms': str(self.ui_config.session_timeout_ms)}

        self.consumer = Consumer(kafka_cfg)
        self.consumer.subscribe([self.topic])
        self.is_emulation_mode = True  # postinit fixed in engine, TODO add in init

    def __del__(self):
        print('Destructor called, consumer deleted.')
        self.consumer.close()

    def process(self, input_json) -> list[Any]:
        """Create list of values from json element

        :return: list constants
        :rtype: list[Any]
        """

        has_is_valid = self.cfg.g_ports_out[1][0].visible
        is_valid = True

        invalid_outputs = [EmptyValue() for p in self.cfg.g_ports_out[0] if p.visible]

        try:
            input_dict = json.loads(input_json)
        except Exception:  # if invalid json
            if has_is_valid:
                invalid_outputs.append(False)
            return invalid_outputs

        outputs = []
        for p in self.cfg.g_ports_out[0]:
            if not p.visible:
                continue

            if p.code == "":
                outputs.append(input_dict)
                continue

            output = jmespath.search(p.code, input_dict)

            if p.required and output is None:
                is_valid = False
                outputs = invalid_outputs
                break

            outputs.append(output)

        if has_is_valid:
            outputs.append(is_valid)
        return outputs

    def _receive_all(self) -> list:
        messages = []
        if self.is_emulation_mode:
            num_messages, timeout = 250, 10
        else:
            num_messages, timeout = 2500, 0.2
        msgs = self.consumer.consume(num_messages=num_messages, timeout=timeout)
        for mes in msgs:
            if mes is not None and mes.error() is None:
                messages.append(mes.value())
        return messages

    def process_many(self, _microbatch) -> list:
        messages = self._receive_all()
        results = []
        for message in messages:
            outputs = self.process(message)
            results.append(outputs)

        print(f'< Kafka {results}')
        return results


class Node(NodesGroup):
    """Class that contains varieties of nodes in class attribute `nodes_array`"""

    type_ = "Kafka"
    nodes_array = [KafkaWrite, KafkaRead]
